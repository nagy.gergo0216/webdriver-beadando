﻿using System;
using System.IO;
using NUnit.Framework;
using OpenQA.Selenium;

namespace WebdriverClass.Tests
{
    class ScreenShotsTestAtClass : TestBase
    {
        [Test]
        public void ScreenShots()
        {
            Driver.Navigate().GoToUrl("http://www.elvira.hu");

            string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            

            //Maximize browser window
            Driver.Manage().Window.Maximize();
            //Create a screenshot and save the file as screenshot.png
            Screenshot screenshot = ((ITakesScreenshot) Driver).GetScreenshot();
            screenshot.SaveAsFile($"{baseDirectory}screenshot.png",ScreenshotImageFormat.Png);
            Assert.IsTrue(File.Exists(baseDirectory + "screenshot.png"));
        }
    }
}
