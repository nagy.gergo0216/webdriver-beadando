﻿using System;
using NUnit.Framework;

namespace WebdriverClass.Tests
{
    class NavigationTest : TestBase
    {
        private String assertTitleChange(String oldTitle) {
            String newTitle = Driver.Title;
            Assert.AreNotEqual(oldTitle, newTitle);
            return newTitle;
        }
        
        [Test]
        public void NavigationExample()
        {
            String title = "";
            
            Driver.Navigate().GoToUrl("https://www.google.com");
            title = assertTitleChange(title);
            
            Driver.Navigate().GoToUrl("https://www.bing.com");
            title = assertTitleChange(title);
            
            Driver.Navigate().Back();
            title = assertTitleChange(title);
            
            Driver.Navigate().Forward();
            title = assertTitleChange(title);
            
            Driver.Navigate().Refresh();
            Assert.AreEqual(title, Driver.Title);
            Assert.AreEqual("https://www.bing.com/", Driver.Url);
        }
    }
}
