﻿using NUnit.Framework;
using OpenQA.Selenium;

namespace WebdriverClass.Tests
{
    class TypingTestAtClass : TestBase
    {
        [Test]
        public void TypeExample()
        {
            Driver.Navigate().GoToUrl("http://www.google.com");
            IWebElement searchField = Driver.FindElement(By.Name("q"));
            
            // Type selenium word to google search field
            searchField.SendKeys("Selenium");
            Assert.AreEqual("Selenium", searchField.GetAttribute("value"));
            
            // Clear google search field
            searchField.Clear();
            Assert.IsEmpty(searchField.GetAttribute("value"));
        }
    }
}
