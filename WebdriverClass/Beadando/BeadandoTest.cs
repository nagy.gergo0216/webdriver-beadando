﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace WebdriverClass.Beadando
{
    class BeadandoTest : TestBase
    {
        [Test]
        public void TheSurveyTests()
        {
            Driver.Navigate().GoToUrl("https://www.surveymonkey.com/r/FNRVK86");

            Driver.FindElement(By.CssSelector(
                    "#question-field-373484282 > fieldset:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(3) > div:nth-child(1)"))
                .Click();
            Assert.IsTrue(Driver.FindElement(By.Id("373484282_2479075470")).Selected);

            Driver.FindElement(By.CssSelector(
                    "#question-field-373480132 > fieldset:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(3) > div:nth-child(1)"))
                .Click();
            Assert.IsTrue(Driver.FindElement(By.Id("373480132_2479050980")).Selected);

            new SelectElement(Driver.FindElement(By.CssSelector(@"#\33 73480133"))).SelectByIndex(2);
            StringAssert.Contains("Webdriver advanced", Driver.FindElement(By.Id("373480133")).Text);

            new SelectElement(Driver.FindElement(By.Id("373480134_2479050988"))).SelectByText("2");
            new SelectElement(Driver.FindElement(By.Id("373480134_2479050997"))).SelectByText("1");
            new SelectElement(Driver.FindElement(By.Id("373480134_2479050998"))).SelectByText("3");
            new SelectElement(Driver.FindElement(By.Id("373480134_2479050999"))).SelectByText("4");
            new SelectElement(Driver.FindElement(By.Id("373480134_2479051000"))).SelectByText("5");
            StringAssert.Contains("2", Driver.FindElement(By.Id("373480134_2479050988")).Text);
            StringAssert.Contains("1", Driver.FindElement(By.Id("373480134_2479050997")).Text);
            StringAssert.Contains("3", Driver.FindElement(By.Id("373480134_2479050998")).Text);
            StringAssert.Contains("4", Driver.FindElement(By.Id("373480134_2479050999")).Text);
            StringAssert.Contains("5", Driver.FindElement(By.Id("373480134_2479051000")).Text);

            Driver.FindElement(By.CssSelector(".btn")).Click();
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(5));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("question-title-373480135")));

            Driver.FindElement(By.CssSelector(
                    "div.answer-option-cell:nth-child(3) > div:nth-child(1) > label:nth-child(2) > span:nth-child(2)"))
                .Click();
            Assert.IsTrue(Driver.FindElement(By.Id("373480135_2479050991")).Selected);

            Driver.FindElement(By.Id("373480136")).SendKeys("Teszt Elek.");
            var actualValue = Driver.FindElement(By.Id("373480136")).GetAttribute("value");
            Assert.IsTrue(actualValue.Contains("Teszt Elek"));

            Driver.FindElement(By.CssSelector("button.btn:nth-child(2)")).Click();
            var title = Driver.Title;
            Assert.IsTrue(title.Contains("Thank you! Create Your Own Online Survey Now!"));
        }

    }
}